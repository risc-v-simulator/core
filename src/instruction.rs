//!
//! ## Unpacking
//!
//! Unpacking functions follow a convention for unpacking immediate data:
//! ```rust,ignore
//! (self.data & /*<32bit mask>*/) >> /*<align to zero>*/ << /*<position in imm>*/;
//! ```
//!

use anyhow::{bail, Result};

use crate::microcode_instruction::MicrocodeInstruction;

const OPCODE_WIDTH: u32 = 7;
const REG_NAME_WIDTH: u32 = 5;
const FUNCT3_WIDTH: u32 = 3;

const OPCODE_MASK: u32 = 0b111_1111;
const REG_NAME_MASK: u32 = 0b11111;
const FUNCT3_MASK: u32 = 0b111;
const FUNCT7_MASK: u32 = 0b111_1111;

pub struct Instruction {
    data: u32,
}

impl Instruction {
    #[must_use]
    pub fn new(data: u32) -> Self {
        Instruction { data }
    }

    #[must_use]
    pub fn get_opcode(&self) -> u32 {
        self.data & OPCODE_MASK
    }

    pub fn get_type(&self) -> Result<Type> {
        match self.get_opcode() {
            0b011_0011 => Ok(Type::R),
            0b001_0011 | 0b000_0011 | 0b110_0111 | 0b111_0011 => Ok(Type::I),
            0b010_0011 => Ok(Type::S),
            0b110_0011 => Ok(Type::B),
            0b110_1111 => Ok(Type::J),
            0b011_0111 | 0b001_0111 => Ok(Type::U),
            _ => bail!("invalid or unimplemented opcode"),
        }
    }

    #[allow(clippy::type_complexity)]
    pub fn unpack_as_r_type(&self) -> Result<(usize, u32, usize, usize, u32)> {
        if self.get_type()? != Type::R {
            bail!("instruction is not of the R type");
        }

        let rd = REG_NAME_MASK & (self.data >> OPCODE_WIDTH);
        let funct3 = FUNCT3_MASK & (self.data >> (OPCODE_WIDTH + REG_NAME_WIDTH));
        let rs1 = REG_NAME_MASK & (self.data >> (OPCODE_WIDTH + REG_NAME_WIDTH + FUNCT3_WIDTH));
        let rs2 = REG_NAME_MASK
            & (self.data >> (OPCODE_WIDTH + REG_NAME_WIDTH + FUNCT3_WIDTH + REG_NAME_WIDTH));
        let funct7 = FUNCT7_MASK
            & (self.data
                >> (OPCODE_WIDTH
                    + REG_NAME_WIDTH
                    + FUNCT3_WIDTH
                    + REG_NAME_WIDTH
                    + REG_NAME_WIDTH));

        Ok((rd as usize, funct3, rs1 as usize, rs2 as usize, funct7))
    }

    pub fn unpack_as_i_type(&self) -> Result<(usize, u32, usize, u32)> {
        if self.get_type()? != Type::I {
            bail!("instruction is not of the I type");
        }

        let rd = REG_NAME_MASK & (self.data >> OPCODE_WIDTH);
        let funct3 = FUNCT3_MASK & (self.data >> (OPCODE_WIDTH + REG_NAME_WIDTH));
        let rs1 = REG_NAME_MASK & (self.data >> (OPCODE_WIDTH + REG_NAME_WIDTH + FUNCT3_WIDTH));

        let imm_11_0 = (self.data & 0b1111_1111_1111_0000_0000_0000_0000_0000) >> 20;
        let imm = sign_extend_u32(imm_11_0, 12);

        Ok((rd as usize, funct3, rs1 as usize, imm))
    }

    pub fn unpack_as_s_type(&self) -> Result<(u32, usize, usize, u32)> {
        if self.get_type()? != Type::S {
            bail!("instruction is not of the S type");
        }

        let funct3 = FUNCT3_MASK & (self.data >> (OPCODE_WIDTH + REG_NAME_WIDTH));
        let rs1 = REG_NAME_MASK & (self.data >> (OPCODE_WIDTH + REG_NAME_WIDTH + FUNCT3_WIDTH));
        let rs2 = REG_NAME_MASK
            & (self.data >> (OPCODE_WIDTH + REG_NAME_WIDTH + FUNCT3_WIDTH + REG_NAME_WIDTH));

        let imm_11_5 = (self.data & 0b1111_1110_0000_0000_0000_0000_0000_0000) >> 25 << 5;
        let imm_4_0 = (self.data & 0b0000_0000_0000_0000_0000_1111_1000_0000) >> 7;
        let imm = sign_extend_u32(imm_11_5 | imm_4_0, 12);

        Ok((funct3, rs1 as usize, rs2 as usize, imm))
    }

    pub fn unpack_as_b_type(&self) -> Result<(u32, usize, usize, u32)> {
        if self.get_type()? != Type::B {
            bail!("instruction is not of the B type");
        }

        let funct3 = FUNCT3_MASK & (self.data >> (OPCODE_WIDTH + REG_NAME_WIDTH));
        let rs1 = REG_NAME_MASK & (self.data >> (OPCODE_WIDTH + REG_NAME_WIDTH + FUNCT3_WIDTH));
        let rs2 = REG_NAME_MASK
            & (self.data >> (OPCODE_WIDTH + REG_NAME_WIDTH + FUNCT3_WIDTH + REG_NAME_WIDTH));

        let imm_12 = (self.data & 0b1000_0000_0000_0000_0000_0000_0000_0000) >> 31 << 12;
        let imm_10_5 = (self.data & 0b0111_1110_0000_0000_0000_0000_0000_0000) >> 25 << 5;
        let imm_4_1 = (self.data & 0b0000_0000_0000_0000_0000_1111_0000_0000) >> 8 << 1;
        let imm_11 = (self.data & 0b0000_0000_0000_0000_0000_0000_1000_0000) >> 7 << 11;
        let imm = sign_extend_u32(imm_12 | imm_10_5 | imm_4_1 | imm_11, 13);

        Ok((funct3, rs1 as usize, rs2 as usize, imm))
    }

    pub fn unpack_as_u_type(&self) -> Result<(usize, u32)> {
        if self.get_type()? != Type::U {
            bail!("instruction is not of the U type");
        }

        let rd = REG_NAME_MASK & (self.data >> OPCODE_WIDTH);

        let imm = self.data & 0b1111_1111_1111_1111_1111_0000_0000_0000;

        Ok((rd as usize, imm))
    }

    pub fn unpack_as_j_type(&self) -> Result<(usize, u32)> {
        if self.get_type()? != Type::J {
            bail!("instruction is not of the J type");
        }

        let rd = REG_NAME_MASK & (self.data >> OPCODE_WIDTH);

        let imm_20 = (self.data & 0b1000_0000_0000_0000_0000_0000_0000_0000) >> 31 << 20;
        let imm_10_1 = (self.data & 0b0111_1111_1110_0000_0000_0000_0000_0000) >> 21 << 1;
        let imm_11 = (self.data & 0b0000_0000_0001_0000_0000_0000_0000_0000) >> 20 << 11;
        let imm_19_12 = (self.data & 0b0000_0000_0000_1111_1111_0000_0000_0000) >> 12 << 12;
        let imm = sign_extend_u32(imm_20 | imm_10_1 | imm_11 | imm_19_12, 21);

        Ok((rd as usize, imm))
    }

    pub fn decode(&self) -> Result<Vec<Vec<MicrocodeInstruction>>> {
        match self.get_opcode() {
            0b011_0011 => self.decode_arithmetics_with_registers(),
            0b001_0011 => self.decode_arithmetics_with_immediate_value(),
            0b000_0011 => self.decode_load(),
            0b010_0011 => self.decode_store(),
            0b110_0011 => self.decode_branching(),
            0b110_1111 => self.decode_jal(),
            0b110_0111 => self.decode_jalr(),
            0b011_0111 => self.decode_lui(),
            0b001_0111 => self.decode_auipc(),
            0b111_0011 => self.decode_ecall(),
            // TODO ebreak
            _ => bail!(
                "invalid or unimplemented instruction {:#09b}",
                self.get_opcode()
            ),
        }
    }

    fn decode_arithmetics_with_registers(&self) -> Result<Vec<Vec<MicrocodeInstruction>>> {
        let (rd, funct3, rs1, rs2, funct7) = self.unpack_as_r_type()?;

        let alu_operation = match (funct3, funct7) {
      (0x0, 0x00) => MicrocodeInstruction::ALUAdd,
      (0x0, 0x20) => MicrocodeInstruction::ALUSub,
      (0x4, 0x00) => MicrocodeInstruction::ALUXor,
      (0x6, 0x00) => MicrocodeInstruction::ALUOr,
      (0x7, 0x00) => MicrocodeInstruction::ALUAnd,
      (0x1, 0x00) => MicrocodeInstruction::ALUShiftLeftLogical,
      (0x5, 0x00) => MicrocodeInstruction::ALUShiftRightLogical,
      (0x5, 0x20) => MicrocodeInstruction::ALUShiftRightArith,
      (0x2, 0x00) => MicrocodeInstruction::ALULessThan,
      (0x3, 0x00) => MicrocodeInstruction::ALULessThanUnsigned,
      _ => bail!("invalid argument for arithmetics with registers (funct3 = {funct3:#3x}, funct7 = {funct7:#4x})")
    };

        Ok(vec![
            vec![
                MicrocodeInstruction::RegisterWriteToBus { register: rs1 },
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::RegisterWriteToBus { register: rs2 },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                alu_operation,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::RegisterReadFromBus { register: rd },
            ],
            vec![MicrocodeInstruction::ProgramCounterIncrement4],
        ])
    }

    fn decode_arithmetics_with_immediate_value(&self) -> Result<Vec<Vec<MicrocodeInstruction>>> {
        let (rd, funct3, rs1, imm) = self.unpack_as_i_type()?;

        let alu_operation = match funct3 {
            0x0 => MicrocodeInstruction::ALUAdd,
            0x4 => MicrocodeInstruction::ALUXor,
            0x6 => MicrocodeInstruction::ALUOr,
            0x7 => MicrocodeInstruction::ALUAnd,
            0x1 => match (imm >> 5) & 0b111_1111 {
                0x00 => MicrocodeInstruction::ALUShiftLeftLogical,
                _ => bail!("invalid imm[11:5] value (imm[11:5] = {})", imm > 5),
            },
            0x5 => match (imm >> 5) & 0b111_1111 {
                0x00 => MicrocodeInstruction::ALUShiftRightLogical,
                0x20 => MicrocodeInstruction::ALUShiftRightArith,
                _ => bail!("invalid shift right type (imm[11:5] = {})", imm > 5),
            },
            0x2 => MicrocodeInstruction::ALULessThan,
            0x3 => MicrocodeInstruction::ALULessThanUnsigned,
            _ => bail!(
                "invalid attribute for arithmetics with immediate value (funct3 = {funct3:#3x})"
            ),
        };

        // shifts are limited to 5-bit imm
        let alu_imm = match funct3 {
            0x1 | 0x5 => imm & 0b11111,
            _ => imm,
        };

        Ok(vec![
            // rd = rs1 <alu_operation> alu_second_input
            vec![
                MicrocodeInstruction::RegisterWriteToBus { register: rs1 },
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::ImmediateWriteToBus { value: alu_imm },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                alu_operation,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::RegisterReadFromBus { register: rd },
            ],
            vec![MicrocodeInstruction::ProgramCounterIncrement4],
        ])
    }

    fn decode_load(&self) -> Result<Vec<Vec<MicrocodeInstruction>>> {
        let (rd, funct3, rs1, imm) = self.unpack_as_i_type()?;

        let (byte_count, memory_width_attribute) = match funct3 {
            0x0 | 0x4 => (1, MicrocodeInstruction::MemoryByte),
            0x1 | 0x5 => (2, MicrocodeInstruction::MemoryHalf),
            0x2 => (4, MicrocodeInstruction::MemoryWord),
            _ => bail!("invalid store width attribute (funct3)"),
        };

        let microcode = vec![
            // memory_pointer = rs1 + sign_extend(imm[11:0])
            vec![
                MicrocodeInstruction::RegisterWriteToBus { register: rs1 },
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::ImmediateWriteToBus { value: imm },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                MicrocodeInstruction::ALUAdd,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::MemoryPointerReadFromBus,
            ],
        ];

        let extend_microcode = match funct3 {
            0x0 | 0x1 => vec![
                // rd = sign_extend(memory[memory_pointer][byte_count*8:0])
                vec![
                    memory_width_attribute,
                    MicrocodeInstruction::MemoryWriteToBus,
                    MicrocodeInstruction::ALUReadFirstFromBus,
                ],
                vec![
                    MicrocodeInstruction::ImmediateWriteToBus {
                        value: 32 - (byte_count * 8),
                    },
                    MicrocodeInstruction::ALUReadSecondFromBus,
                ],
                vec![
                    MicrocodeInstruction::ALUShiftLeftLogical,
                    MicrocodeInstruction::ALUWriteToBus,
                    MicrocodeInstruction::ALUReadFirstFromBus,
                ],
                vec![
                    MicrocodeInstruction::ALUShiftRightArith,
                    MicrocodeInstruction::ALUWriteToBus,
                    MicrocodeInstruction::RegisterReadFromBus { register: rd },
                ],
            ],
            0x2 | 0x4 | 0x5 => vec![
                // rd = memory[memory_pointer]
                vec![
                    memory_width_attribute,
                    MicrocodeInstruction::MemoryWriteToBus,
                    MicrocodeInstruction::RegisterReadFromBus { register: rd },
                ],
            ],
            _ => bail!("invalid store width attribute (funct3)"),
        };

        Ok([
            microcode,
            extend_microcode,
            vec![vec![MicrocodeInstruction::ProgramCounterIncrement4]],
        ]
        .concat())
    }

    fn decode_store(&self) -> Result<Vec<Vec<MicrocodeInstruction>>> {
        let (funct3, rs1, rs2, imm) = self.unpack_as_s_type()?;

        let memory_attribute = match funct3 {
            0x0 => MicrocodeInstruction::MemoryByte,
            0x1 => MicrocodeInstruction::MemoryHalf,
            0x2 => MicrocodeInstruction::MemoryWord,
            _ => bail!("invalid store width attribute (funct3)"),
        };

        Ok(vec![
            // memory_pointer = rs1 + sign_extend(imm[11:0])
            vec![
                MicrocodeInstruction::RegisterWriteToBus { register: rs1 },
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::ImmediateWriteToBus { value: imm },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                MicrocodeInstruction::ALUAdd,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::MemoryPointerReadFromBus,
            ],
            // memory[memory_pointer][memory_attribute:0] = rs2[memory_attribute:0]
            vec![
                MicrocodeInstruction::RegisterWriteToBus { register: rs2 },
                memory_attribute,
                MicrocodeInstruction::MemoryReadFromBus,
            ],
            vec![MicrocodeInstruction::ProgramCounterIncrement4],
        ])
    }

    fn decode_branching(&self) -> Result<Vec<Vec<MicrocodeInstruction>>> {
        let (funct3, rs1, rs2, imm) = self.unpack_as_b_type()?;

        let alu_operation = match funct3 {
            0x0 => MicrocodeInstruction::ALUEquals,
            0x1 => MicrocodeInstruction::ALUNotEquals,
            0x4 => MicrocodeInstruction::ALULessThan,
            0x5 => MicrocodeInstruction::ALUGreaterOrEqual,
            0x6 => MicrocodeInstruction::ALULessThanUnsigned,
            0x7 => MicrocodeInstruction::ALUGreaterOrEqualUnsigned,
            _ => bail!("invalid ALU operation (funct3)"),
        };

        Ok(vec![
            // compare rs1 and rs2 and exit microcode on negative result
            vec![
                MicrocodeInstruction::RegisterWriteToBus { register: rs1 },
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::RegisterWriteToBus { register: rs2 },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                alu_operation,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::OnBus0ProgramCounterIncrement4AndExitMicrocode,
            ],
            // pc += sign_extend(imm)
            vec![
                MicrocodeInstruction::ProgramCounterWriteToBus,
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::ImmediateWriteToBus { value: imm },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                MicrocodeInstruction::ALUAdd,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::ProgramCounterReadFromBus,
            ],
        ])
    }

    fn decode_jal(&self) -> Result<Vec<Vec<MicrocodeInstruction>>> {
        let (rd, imm) = self.unpack_as_j_type()?;

        Ok(vec![
            // temporary_internal_register = pc
            vec![
                MicrocodeInstruction::ProgramCounterWriteToBus,
                MicrocodeInstruction::TemporaryInternalRegisterReadFromBus,
            ],
            // pc = pc + sign_extend(imm[20:1])
            vec![
                MicrocodeInstruction::ProgramCounterWriteToBus,
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::ImmediateWriteToBus { value: imm },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                MicrocodeInstruction::ALUAdd,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::ProgramCounterReadFromBus,
            ],
            // rd = temporary_internal_register + 4
            vec![
                MicrocodeInstruction::TemporaryInternalRegisterWriteToBus,
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::ImmediateWriteToBus { value: 4 },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                MicrocodeInstruction::ALUAdd,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::RegisterReadFromBus { register: rd },
            ],
        ])
    }

    fn decode_jalr(&self) -> Result<Vec<Vec<MicrocodeInstruction>>> {
        let (rd, funct3, rs1, imm) = self.unpack_as_i_type()?;

        if funct3 != 0x0 {
            bail!("invalid funct3 value (funct3 = {funct3:#3x})");
        }

        Ok(vec![
            // temporary_internal_register = pc
            vec![
                MicrocodeInstruction::ProgramCounterWriteToBus,
                MicrocodeInstruction::TemporaryInternalRegisterReadFromBus,
            ],
            // pc = rs1 + sign_extend(imm[11:0])
            vec![
                MicrocodeInstruction::RegisterWriteToBus { register: rs1 },
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::ImmediateWriteToBus { value: imm },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                MicrocodeInstruction::ALUAdd,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::ImmediateWriteToBus {
                    value: 0b1111_1111_1111_1111_1111_1111_1111_1110,
                },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                MicrocodeInstruction::ALUAnd,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::ProgramCounterReadFromBus,
            ],
            // rd = temporary_internal_register + 4
            vec![
                MicrocodeInstruction::TemporaryInternalRegisterWriteToBus,
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::ImmediateWriteToBus { value: 4 },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                MicrocodeInstruction::ALUAdd,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::RegisterReadFromBus { register: rd },
            ],
        ])
    }

    fn decode_lui(&self) -> Result<Vec<Vec<MicrocodeInstruction>>> {
        let (rd, imm) = self.unpack_as_u_type()?;

        Ok(vec![
            // rd = imm
            vec![
                MicrocodeInstruction::ImmediateWriteToBus { value: imm },
                MicrocodeInstruction::RegisterReadFromBus { register: rd },
            ],
            vec![MicrocodeInstruction::ProgramCounterIncrement4],
        ])
    }

    fn decode_auipc(&self) -> Result<Vec<Vec<MicrocodeInstruction>>> {
        let (rd, imm) = self.unpack_as_u_type()?;

        Ok(vec![
            // rd = pc + imm
            vec![
                MicrocodeInstruction::ProgramCounterWriteToBus,
                MicrocodeInstruction::ALUReadFirstFromBus,
            ],
            vec![
                MicrocodeInstruction::ImmediateWriteToBus { value: imm },
                MicrocodeInstruction::ALUReadSecondFromBus,
            ],
            vec![
                MicrocodeInstruction::ALUAdd,
                MicrocodeInstruction::ALUWriteToBus,
                MicrocodeInstruction::RegisterReadFromBus { register: rd },
            ],
            vec![MicrocodeInstruction::ProgramCounterIncrement4],
        ])
    }

    fn decode_ecall(&self) -> Result<Vec<Vec<MicrocodeInstruction>>> {
        let (_, funct3, _, imm) = self.unpack_as_i_type()?;

        if funct3 != 0x0 {
            bail!("invalid funct3 value (funct3 = {funct3:#3x})");
        }

        if imm != 0x0 {
            bail!("invalid imm value");
        }

        Ok(vec![
            vec![MicrocodeInstruction::Ecall],
            vec![MicrocodeInstruction::ProgramCounterIncrement4],
        ])
    }
}

#[allow(clippy::cast_possible_wrap, clippy::cast_sign_loss)]
fn sign_extend_u32(number: u32, actual_bit_count: u32) -> u32 {
    assert!(
        actual_bit_count > 0 && actual_bit_count <= 32,
        "the actual bit count must be >0 and <=32"
    );

    let bits = 32 - actual_bit_count;

    let shifted_imm = (number << bits) as i32;
    (shifted_imm >> bits) as u32
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Type {
    R,
    I,
    S,
    B,
    U,
    J,
}

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod tests {
    use super::*;
    use test_case::test_case;

    #[test_case(0x05d0_0893, (17, 0x0, 0, 93); "addi a7, zero, 93")]
    fn test_read_i_type(data: u32, expected: (usize, u32, usize, u32)) {
        let instruction = Instruction::new(data);
        let actual = instruction.unpack_as_i_type().unwrap();
        assert_eq!(expected, actual);
    }

    #[test_case(0x2340_10ef, (1, 0x1234); "jal ra, 122e8")]
    fn test_j_instruction(data: u32, expected: (usize, u32)) {
        let instruction = Instruction::new(data);
        let actual = instruction.unpack_as_j_type().unwrap();
        assert_eq!(expected, actual);
    }
}
