#![deny(clippy::todo)]
#![allow(clippy::missing_errors_doc)] // we don't need this for now
#![allow(clippy::module_name_repetitions)]

pub mod instruction;
pub mod memory;
pub mod microcode_instruction;
pub mod simulator;
