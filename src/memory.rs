use anyhow::{bail, Result};
use std::ops::Range;

#[derive(Debug)]
pub struct Segment {
    pub start_address: usize,
    pub data: Vec<u8>,
    pub write_access: bool,
}

#[derive(Debug, Default)]
pub struct Memory {
    segments: Vec<Segment>,
}

impl Memory {
    #[must_use]
    pub fn new() -> Self {
        Memory {
            segments: Vec::new(),
        }
    }

    pub fn put(&mut self, start_address: usize, data: Vec<u8>, write_access: bool) -> Result<()> {
        if let Some(previous_segment_index) =
            self.find_index_of_segment_with_greatest_address_lower_than_or_equal_to(start_address)
        {
            let previous_segment = &self.segments[previous_segment_index];
            if previous_segment.start_address + previous_segment.data.len() > start_address {
                bail!("segment overlaps with the previous one");
            }

            let next_segment_index = previous_segment_index + 1;
            if next_segment_index < self.segments.len() {
                let next_segment = &self.segments[next_segment_index];
                if start_address + data.len() > next_segment.start_address {
                    bail!("segment overlaps with the following one");
                }
            }

            self.segments.insert(
                previous_segment_index + 1,
                Segment {
                    start_address,
                    data,
                    write_access,
                },
            );
        } else if self.segments.is_empty() {
            self.segments.push(Segment {
                start_address,
                data,
                write_access,
            });
        } else {
            let next_segment = &self.segments[0];
            if start_address + data.len() > next_segment.start_address {
                bail!("segment overlaps with the following one");
            }

            self.segments.insert(
                0,
                Segment {
                    start_address,
                    data,
                    write_access,
                },
            );
        }

        Ok(())
    }

    #[must_use]
    pub fn get(&self, start_address: usize) -> Option<&Segment> {
        self.segments
            .iter()
            .find(|segment| segment.start_address == start_address)
    }

    #[must_use]
    pub fn read_range(&self, range: Range<usize>) -> Vec<u8> {
        let mut range_reader = RangeReader::new(self, range);
        range_reader.read();
        range_reader.output
    }

    pub fn write_at(&mut self, address: usize, data: &[u8]) -> Result<()> {
        if let Some(segment_index) =
            self.find_index_of_segment_with_greatest_address_lower_than_or_equal_to(address)
        {
            let segment = &mut self.segments[segment_index];
            if !segment.write_access {
                bail!("memory segment is not writeable");
            }

            if address + data.len() <= segment.start_address + segment.data.len() {
                let position_in_segment = address - segment.start_address;
                segment.data.splice(
                    position_in_segment..position_in_segment + data.len(),
                    data.iter().copied(),
                );
                Ok(())
            } else {
                bail!("data buffer is larger than the memory segment");
            }
        } else {
            bail!("no writeable memory segment found");
        }
    }

    fn find_index_of_segment_with_greatest_address_lower_than_or_equal_to(
        &self,
        address: usize,
    ) -> Option<usize> {
        self.segments
            .iter()
            .enumerate()
            .rev()
            .find(|(_, segment)| segment.start_address <= address)
            .map(|(i, _)| i)
    }
}

struct RangeReader<'a> {
    memory: &'a Memory,
    range: Range<usize>,
    current_position: usize,
    output: Vec<u8>,
}

impl<'a> RangeReader<'a> {
    fn new(memory: &'a Memory, range: Range<usize>) -> Self {
        RangeReader {
            memory,
            current_position: range.start,
            output: Vec::with_capacity(range.end - range.start),
            range,
        }
    }

    fn read(&mut self) {
        while self.current_position < self.range.end {
            if let Some(segment_index) = self
                .memory
                .find_index_of_segment_with_greatest_address_lower_than_or_equal_to(
                    self.current_position,
                )
            {
                self.append_data_from_segment(segment_index);

                let next_segment_index = segment_index + 1;
                if next_segment_index < self.memory.segments.len() {
                    self.append_zero_bytes_up_to_next_segment(next_segment_index);
                } else {
                    self.append_zero_bytes_until_the_end();
                }
            } else if self.memory.segments.is_empty() {
                self.append_zero_bytes_until_the_end();
            } else {
                self.append_zero_bytes_up_to_next_segment(0);
            }
        }
    }

    fn append_data_from_segment(&mut self, segment_index: usize) {
        let segment = &self.memory.segments[segment_index];
        let start_position_in_segment = self.current_position - segment.start_address;
        if start_position_in_segment < segment.data.len() {
            let bytes_to_read = usize::min(
                segment.data.len() - start_position_in_segment,
                self.range.end - self.current_position,
            );
            self.output.extend_from_slice(
                &segment.data[start_position_in_segment..start_position_in_segment + bytes_to_read],
            );
            self.current_position += bytes_to_read;
        }
    }

    fn append_zero_bytes_up_to_next_segment(&mut self, next_segment_index: usize) {
        self.append_zero_bytes(usize::min(
            self.memory.segments[next_segment_index].start_address - self.current_position,
            self.range.end - self.current_position,
        ));
    }

    fn append_zero_bytes_until_the_end(&mut self) {
        self.append_zero_bytes(self.range.end - self.current_position);
    }

    fn append_zero_bytes(&mut self, count: usize) {
        self.output.resize(self.output.len() + count, 0);
        self.current_position += count;
    }
}
