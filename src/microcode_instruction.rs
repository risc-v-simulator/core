#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MicrocodeInstruction {
    ProgramCounterWriteToBus,
    ProgramCounterReadFromBus,
    ProgramCounterIncrement4,

    MemoryPointerReadFromBus,
    MemoryReadFromBus,
    MemoryWriteToBus,
    MemoryByte,
    MemoryHalf,
    MemoryWord,

    TemporaryInternalRegisterWriteToBus,
    TemporaryInternalRegisterReadFromBus,

    RegisterWriteToBus { register: usize },
    RegisterReadFromBus { register: usize },

    ALUReadFirstFromBus,
    ALUReadSecondFromBus,
    ALUWriteToBus,
    ALUAdd,
    ALUSub,
    ALUXor,
    ALUOr,
    ALUAnd,
    ALUShiftLeftLogical,
    ALUShiftRightLogical,
    ALUShiftRightArith,

    ALUEquals,
    ALUNotEquals,
    ALULessThan,
    ALUGreaterOrEqual,
    ALULessThanUnsigned,
    ALUGreaterOrEqualUnsigned,

    ImmediateWriteToBus { value: u32 },

    Ecall,

    OnBus0ProgramCounterIncrement4AndExitMicrocode,
}

impl MicrocodeInstruction {
    #[must_use]
    pub fn get_type(&self) -> Type {
        match &self {
            Self::ProgramCounterReadFromBus
            | Self::MemoryPointerReadFromBus
            | Self::MemoryReadFromBus
            | Self::TemporaryInternalRegisterReadFromBus
            | Self::ALUReadFirstFromBus
            | Self::ALUReadSecondFromBus
            | Self::RegisterReadFromBus { register: _ } => Type::Read,
            Self::ProgramCounterWriteToBus
            | Self::MemoryWriteToBus
            | Self::ALUWriteToBus
            | Self::TemporaryInternalRegisterWriteToBus
            | Self::RegisterWriteToBus { register: _ }
            | Self::ImmediateWriteToBus { value: _ } => Type::Write,
            Self::ProgramCounterIncrement4 | Self::Ecall => Type::NoInteraction,
            Self::MemoryByte
            | Self::MemoryHalf
            | Self::MemoryWord
            | Self::ALUAdd
            | Self::ALUSub
            | Self::ALUXor
            | Self::ALUOr
            | Self::ALUAnd
            | Self::ALUShiftLeftLogical
            | Self::ALUShiftRightLogical
            | Self::ALUShiftRightArith
            | Self::ALUEquals
            | Self::ALUNotEquals
            | Self::ALULessThan
            | Self::ALUGreaterOrEqual
            | Self::ALULessThanUnsigned
            | Self::ALUGreaterOrEqualUnsigned => Type::Attribute,
            Self::OnBus0ProgramCounterIncrement4AndExitMicrocode => Type::Conditional,
        }
    }

    #[must_use]
    pub fn is_memory_width_attribute(&self) -> bool {
        matches!(self, Self::MemoryByte | Self::MemoryHalf | Self::MemoryWord)
    }

    #[must_use]
    pub fn is_alu_operation_attribute(&self) -> bool {
        matches!(
            self,
            Self::ALUAdd
                | Self::ALUSub
                | Self::ALUXor
                | Self::ALUOr
                | Self::ALUAnd
                | Self::ALUShiftLeftLogical
                | Self::ALUShiftRightLogical
                | Self::ALUShiftRightArith
                | Self::ALUEquals
                | Self::ALUNotEquals
                | Self::ALULessThan
                | Self::ALUGreaterOrEqual
                | Self::ALULessThanUnsigned
                | Self::ALUGreaterOrEqualUnsigned
        )
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum Type {
    Read,
    Write,
    NoInteraction,
    Attribute,
    Conditional,
}
