use anyhow::{anyhow, bail, Result};
use std::{cell::RefCell, io, mem::size_of, rc::Rc};

use crate::{
    instruction::Instruction,
    memory::Memory,
    microcode_instruction::{self, MicrocodeInstruction},
};

pub struct Simulator {
    memory: Rc<RefCell<Memory>>,
    program_counter: u32,
    memory_pointer: u32,
    temporary_internal_register: u32,
    bus: u32,
    alu_first_input: u32,
    alu_second_input: u32,
    registers: [u32; 32],
    status: SimulatorStatus,
    stdout: Box<dyn io::Write>,
}

impl Simulator {
    #[must_use]
    pub fn new(
        memory: Rc<RefCell<Memory>>,
        entry_address: u32,
        stack_pointer: u32,
        stdout: Box<dyn io::Write>,
    ) -> Self {
        let mut registers = [0; 32];
        registers[2] = stack_pointer;

        Simulator {
            memory,
            program_counter: entry_address,
            memory_pointer: 0,
            temporary_internal_register: 0,
            bus: 0,
            alu_first_input: 0,
            alu_second_input: 0,
            registers,
            status: SimulatorStatus::Running,
            stdout,
        }
    }

    #[must_use]
    pub fn get_memory(&self) -> Rc<RefCell<Memory>> {
        self.memory.clone()
    }

    #[must_use]
    pub fn get_program_counter(&self) -> u32 {
        self.program_counter
    }

    #[must_use]
    pub fn get_memory_pointer(&self) -> u32 {
        self.memory_pointer
    }

    #[must_use]
    pub fn get_bus(&self) -> u32 {
        self.bus
    }

    #[must_use]
    pub fn get_alu_first_input(&self) -> u32 {
        self.alu_first_input
    }

    #[must_use]
    pub fn get_alu_second_input(&self) -> u32 {
        self.alu_second_input
    }

    #[must_use]
    pub fn get_registers(&self) -> [u32; 32] {
        self.registers
    }

    #[must_use]
    pub fn get_status(&self) -> SimulatorStatus {
        self.status
    }

    pub fn fetch(&self) -> Result<Instruction> {
        let instruction_position: usize = self.program_counter.try_into()?;
        let instruction_components: [u8; 4] = {
            self.memory
                .try_borrow()?
                .read_range(instruction_position..instruction_position + 4)
                .try_into()
                .map_err(|_| anyhow!("Read invalid amount of data"))?
        };
        let instruction_data = u32::from_le_bytes(instruction_components);

        Ok(Instruction::new(instruction_data))
    }

    pub fn execute(&mut self, microcode: &Vec<Vec<MicrocodeInstruction>>) -> Result<()> {
        for microcode_step in microcode {
            let exit_microcode = self.execute_step(microcode_step)?;
            if exit_microcode {
                break;
            }
        }

        Ok(())
    }

    pub fn execute_step(&mut self, microcode_step: &[MicrocodeInstruction]) -> Result<bool> {
        self.execute_instructions_writing_to_bus(microcode_step)?;
        self.execute_instructions_reading_from_bus(microcode_step)?;
        self.execute_instructions_not_interacting_with_bus(microcode_step)?;
        let exit_microcode = self.execute_conditional_instructions(microcode_step)?;

        Ok(exit_microcode)
    }

    #[rustfmt::skip]
    fn execute_instructions_writing_to_bus(
        &mut self,
        microcode_instructions: &[MicrocodeInstruction],
    ) -> Result<()> {
        microcode_instructions
            .iter()
            .filter(|&microcode_instruction| {
                microcode_instruction.get_type() == microcode_instruction::Type::Write
            })
            .try_for_each(|microcode_instruction| {
                self.bus = match microcode_instruction {
                    MicrocodeInstruction::ProgramCounterWriteToBus => self.program_counter,
                    MicrocodeInstruction::MemoryWriteToBus => self.execute_memory_read(
                        Self::find_memory_width_attribute(microcode_instructions)?,
                    )?,
                    MicrocodeInstruction::TemporaryInternalRegisterWriteToBus => self.temporary_internal_register,
                    MicrocodeInstruction::RegisterWriteToBus { register } => self.registers[*register],
                    MicrocodeInstruction::ALUWriteToBus => self.execute_alu_calculation(
                        Self::find_alu_operation_attribute(microcode_instructions)?,
                    )?,
                    MicrocodeInstruction::ImmediateWriteToBus { value: imm } => *imm,
                    _ => bail!("unimplemented write microcode instruction")
                };

                Ok(())
            })?;

        Ok(())
    }

    #[rustfmt::skip]
    fn execute_instructions_reading_from_bus(
        &mut self,
        microcode_instructions: &[MicrocodeInstruction]
    ) -> Result<()> {
        microcode_instructions
            .iter()
            .filter(|&microcode_instruction| microcode_instruction.get_type() == microcode_instruction::Type::Read)
            .try_for_each(|microcode_instruction| {
                match microcode_instruction {
                    MicrocodeInstruction::ProgramCounterReadFromBus => self.program_counter = self.bus,
                    MicrocodeInstruction::MemoryPointerReadFromBus => self.memory_pointer = self.bus,
                    MicrocodeInstruction::MemoryReadFromBus => self.execute_memory_write(
                        Self::find_memory_width_attribute(microcode_instructions)?,
                        self.bus,
                    )?,
                    MicrocodeInstruction::TemporaryInternalRegisterReadFromBus => self.temporary_internal_register = self.bus,
                    MicrocodeInstruction::RegisterReadFromBus { register } => {
                        if *register != 0 {
                            self.registers[*register] = self.bus;
                        }
                    },
                    MicrocodeInstruction::ALUReadFirstFromBus => self.alu_first_input = self.bus,
                    MicrocodeInstruction::ALUReadSecondFromBus => self.alu_second_input = self.bus,
                    _ => bail!("unimplemented read microcode instruction"),
                };

                Ok(())
            })?;

        Ok(())
    }

    fn execute_instructions_not_interacting_with_bus(
        &mut self,
        microcode_instructions: &[MicrocodeInstruction],
    ) -> Result<()> {
        microcode_instructions
            .iter()
            .filter(|&microcode_instruction| {
                microcode_instruction.get_type() == microcode_instruction::Type::NoInteraction
            })
            .try_for_each(|microcode_instruction| {
                match microcode_instruction {
                    MicrocodeInstruction::ProgramCounterIncrement4 => self.program_counter += 4,
                    MicrocodeInstruction::Ecall => self.execute_ecall()?,
                    _ => bail!("unimplemented no_interaction microcode instruction"),
                };

                Ok(())
            })?;

        Ok(())
    }

    #[allow(clippy::unnecessary_wraps)]
    fn execute_conditional_instructions(
        &mut self,
        microcode_instructions: &[MicrocodeInstruction],
    ) -> Result<bool> {
        // there is only one conditional instruction
        if microcode_instructions.iter().any(|&microcode_instruction| {
            microcode_instruction
                == MicrocodeInstruction::OnBus0ProgramCounterIncrement4AndExitMicrocode
        }) && self.bus == 0
        {
            self.program_counter += 4;
            return Ok(true);
        }

        Ok(false)
    }

    fn find_memory_width_attribute(
        microcode_instructions: &[MicrocodeInstruction],
    ) -> Result<&MicrocodeInstruction> {
        microcode_instructions
            .iter()
            .find(|&microcode_instruction| microcode_instruction.is_memory_width_attribute())
            .ok_or(anyhow!("no memory width attribute in the microcode step"))
    }

    fn find_alu_operation_attribute(
        microcode_instructions: &[MicrocodeInstruction],
    ) -> Result<&MicrocodeInstruction> {
        microcode_instructions
            .iter()
            .find(|&microcode_instruction| microcode_instruction.is_alu_operation_attribute())
            .ok_or(anyhow!("no ALU operation attribute in the microcode step"))
    }

    fn map_memory_width_attribute_to_byte_count(
        memory_width_attribute: &MicrocodeInstruction,
    ) -> Result<usize> {
        match memory_width_attribute {
            MicrocodeInstruction::MemoryByte => Ok(1),
            MicrocodeInstruction::MemoryHalf => Ok(2),
            MicrocodeInstruction::MemoryWord => Ok(4),
            _ => bail!("unimplemented memory width attribute"),
        }
    }

    #[allow(clippy::cast_possible_wrap, clippy::cast_sign_loss)]
    #[rustfmt::skip]
    fn execute_alu_calculation(
        &mut self,
        alu_operation_attribute: &MicrocodeInstruction,
    ) -> Result<u32> {
        Ok(match alu_operation_attribute {
            MicrocodeInstruction::ALUAdd => self.alu_first_input.wrapping_add(self.alu_second_input),
            MicrocodeInstruction::ALUSub => self.alu_first_input.wrapping_sub(self.alu_second_input),
            MicrocodeInstruction::ALUXor => self.alu_first_input ^ self.alu_second_input,
            MicrocodeInstruction::ALUOr => self.alu_first_input | self.alu_second_input,
            MicrocodeInstruction::ALUAnd => self.alu_first_input & self.alu_second_input,
            MicrocodeInstruction::ALUShiftLeftLogical => self.alu_first_input << self.alu_second_input,
            MicrocodeInstruction::ALUShiftRightLogical => self.alu_first_input >> self.alu_second_input,
            MicrocodeInstruction::ALUShiftRightArith => (self.alu_first_input as i32 >> self.alu_second_input) as u32,
            MicrocodeInstruction::ALUEquals => u32::from(self.alu_first_input == self.alu_second_input),
            MicrocodeInstruction::ALUNotEquals => u32::from(self.alu_first_input != self.alu_second_input),
            MicrocodeInstruction::ALULessThan => u32::from((self.alu_first_input as i32) < (self.alu_second_input as i32)),
            MicrocodeInstruction::ALUGreaterOrEqual => u32::from((self.alu_first_input as i32) >= (self.alu_second_input as i32)),
            MicrocodeInstruction::ALULessThanUnsigned => u32::from(self.alu_first_input < self.alu_second_input),
            MicrocodeInstruction::ALUGreaterOrEqualUnsigned => u32::from(self.alu_first_input >= self.alu_second_input),
            _ => bail!("unimplemented ALU operation attribute"),
        })
    }

    fn execute_memory_read(
        &mut self,
        memory_width_attribute: &MicrocodeInstruction,
    ) -> Result<u32> {
        let byte_count = Self::map_memory_width_attribute_to_byte_count(memory_width_attribute)?;

        let memory_pointer = self.memory_pointer as usize;

        let buffer = {
            let mut buffer = [0u8; size_of::<u32>()];
            for (i, &byte) in self
                .memory
                .try_borrow()?
                .read_range(memory_pointer..memory_pointer + byte_count)
                .iter()
                .enumerate()
            {
                buffer[i] = byte;
            }
            buffer
        };
        let read_value = u32::from_le_bytes(buffer);

        Ok(read_value)
    }

    fn execute_memory_write(
        &mut self,
        memory_width_attribute: &MicrocodeInstruction,
        bus: u32,
    ) -> Result<()> {
        let byte_count = Self::map_memory_width_attribute_to_byte_count(memory_width_attribute)?;

        let buffer = bus.to_le_bytes();
        let slice = &buffer[..byte_count];

        let memory_pointer: usize = self.memory_pointer.try_into()?;
        {
            self.memory
                .try_borrow_mut()?
                .write_at(memory_pointer, slice)?;
        }

        Ok(())
    }

    fn execute_ecall(&mut self) -> Result<()> {
        let a7_value = self.registers[17];
        match a7_value {
            64 => {
                let a0_value = self.registers[10];
                if a0_value != 1 {
                    bail!("ecall currently only supports a0 = 1 (stdout)");
                }

                let a1_value = self.registers[11];
                let a2_value = self.registers[12];

                let text_start: usize = a1_value.try_into()?;
                let text_end: usize = (a1_value + a2_value).try_into()?;
                let text_data = {
                    let memory = self.memory.try_borrow()?;
                    memory.read_range(text_start..text_end)
                };

                self.stdout.write_all(&text_data)?;
            }
            93 => {
                let a0_value = self.registers[10];
                self.status = SimulatorStatus::Exited { code: a0_value }
            }
            _ => bail!("ecall currently only supports a7 = 64 or 93"),
        }

        Ok(())
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum SimulatorStatus {
    Running,
    Exited { code: u32 },
}
